SHELL:=/bin/bash

DESTDIR=

BINDIR=/usr/bin
MANDIR=/usr/share/man/man1
SHAREDIR=/usr/share/backuppc-silzigan
COMPDIR=/etc/bash_completion.d

.PHONY: all install update sync upload help clean pkg pages

all:
	pod2man backuppc-silzigan | gzip > backuppc-silzigan.1.gz
	pod2html --css podstyle.css --index --header backuppc-silzigan > backuppc-silzigan.html
	pod2man backuppc-cleanold | gzip > backuppc-cleanold.1.gz
	pod2html --css podstyle.css --index --header backuppc-cleanold > backuppc-cleanold.html
	pod2man backuppc-movehome | gzip > backuppc-movehome.1.gz
	pod2html --css podstyle.css --index --header backuppc-movehome > backuppc-movehome.html

install: update

update:
	@install -d -m 0755 -o root -g root $(DESTDIR)/$(SHAREDIR)
	@install -d -m 0755 -o root -g root $(DESTDIR)/$(MANDIR)
	@install -d -m 0755 -o root -g root $(DESTDIR)/$(COMPDIR)

	install    -m 0755 -o root -g root backuppc-silzigan $(DESTDIR)/$(BINDIR)
	install    -m 0755 -o root -g root backuppc-cleanold $(DESTDIR)/$(BINDIR)
	install    -m 0755 -o root -g root backuppc-movehome $(DESTDIR)/$(BINDIR)

	install    -m 0644 -o root -g root backuppc-silzigan.1.gz $(DESTDIR)/$(MANDIR)
	install    -m 0644 -o root -g root backuppc-cleanold.1.gz $(DESTDIR)/$(MANDIR)
	install    -m 0644 -o root -g root backuppc-movehome.1.gz $(DESTDIR)/$(MANDIR)

	install    -m 0644 -o root -g root exclude.sample.txt $(DESTDIR)/$(SHAREDIR)
	install    -m 0644 -o root -g root backuppc-silzigan.sample.cron $(DESTDIR)/$(SHAREDIR)

	install    -m 0644 -o root -g root backuppc-silzigan.bash_completion $(DESTDIR)/$(COMPDIR)/backuppc-silzigan

sync:
	git pull

pkg: all
	./make-package-debian

pages: all pkg
	mkdir -p public/download
	cp -p *.html       public/
	cp -p podstyle.css public/
	cp -p LICENSE.md   public/
	cp -p --no-clobber backuppc-silzigan_*_all.deb  public/download/
	cd public; ln -sf backuppc-silzigan.html index.html
	echo '<html><body><h1>BackupPC-Silzigan Debian Package</h1><ul>' > public/download/index.html
	(cd public/download; while read file; do printf '<li><a href="%s">%s</a> (%s)</li>\n' $$file $$file $$(stat -c %y $$file | cut -f 1 -d ' '); done < <(ls -1t *.deb) >> index.html)
	echo '</ul></body></html>' >> public/download/index.html

clean:
	@rm backuppc-silzigan_*_all.deb backuppc-silzigan.1.gz backuppc-silzigan.html pod2htmd.tmp

help:
	@echo "Possibles targets:"
	@echo " * all     : make manual"
	@echo " * install : complete install"
	@echo " * update  : update install (do not update cron file)"
	@echo " * sync    : sync with official repository"
	@echo " * upload  : upload on public dav forge space"
	@echo " * pkg     : build Debian package"

