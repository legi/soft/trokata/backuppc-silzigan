# Backuppc-Silzigan - Cutting the backup of computers into small pieces

## Description

[BackupPC](http://backuppc.sourceforge.net/) is a very good backup software.
However, it has a few shortcomings.
One of them is the size of the backups.
Currently, BackupPC is slow on large volumes
and the consolidation it does between incremental and full backups takes a long time.
Furthermore, when backing up HOMEs,
it is not possible that each user manages his own HOME...

The technique used in this program was initiated in the LEGI laboratory by Patrick Bégou,
and then generalized by Gabriel Moreau via a Perl mill: ```backuppc-silzigan```.
Silzigan means "to sausage" in Breton.

We create a backup file for BackupPC by user folder for each machine to save.
This user is associated with this backup so he can go and see what is in it.
If there are 150 users on the machine
the backup will be cut into 150 pieces,
each of which will not take hours but usually minutes.
As BackupPC consolidates once a week (will no longer be true in the next version),
this consolidation is also distributed.

As there is not only one server or workstation to save
all the pieces of all the computers are sorted randomly.
Thus, all the machines are saved a little all the time
but none of them takes all the space at a given moment...

All the command help and description is on the online manual
[BackupPC-Silzigan](https://legi.gricad-pages.univ-grenoble-alpes.fr/soft/trokata/backuppc-silzigan/).

Another command allows you to delete old backups after a period of inactivity
in order to comply with the European GDPR
[BackupPC-CleanOld](https://legi.gricad-pages.univ-grenoble-alpes.fr/soft/trokata/backuppc-silzigan/backuppc-cleanold.html).

Another command allows you to rename all users folder of the form `server1_userX` to `server2_userX` inside the BackupPC datafile and update all the configuration file
[BackupPC-MoveHome](https://legi.gricad-pages.univ-grenoble-alpes.fr/soft/trokata/backuppc-silzigan/backuppc-movehome.html).


## Download - Debian package

Debian is a GNU/Linux distribution.
Debian (and certainly Ubuntu) package for amd64 arch could be download on: https://legi.gricad-pages.univ-grenoble-alpes.fr/soft/trokata/backuppc-silzigan/download..

You can then install it with

```bash
sudo dpkg -i backuppc-silzigan_*_amd64.deb
```
(just replace * with the version you have donwloaded).


## Repository / Contribute

### Source

The whole code is under **free license**.
The script in ```bash``` or ```Perl``` is under GPL version 2 or more recent (http://www.gnu.org/licenses/gpl.html).
All the source code is available on the forge of the Grenoble campus:
https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/backuppc-silzigan
The sources are managed via Git (GitLab).
It is very easy to stay synchronized with these sources.

* The initial recovery
  ```bash
  git clone https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/backuppc-silzigan
  ```
* The updates afterwards
  ```bash
  git pull
  ```
* Contribute.
  It is possible to contribute by proposing pull requests,
  merge requests or simply old fashioned patches.

### Patch

It is possible to have a writing access to the project on the forge
on motivated request to [Gabriel Moreau](mailto:Gabriel.Moreau_A_legi.grenoble-inp.fr).
For questions of administration time and security,
the project is not directly accessible in writing without authorization.
For questions of decentralization of the web, of autonomy
and non-allegiance to the ambient (and North American) centralism,
we use the forge of the university campus of Grenoble...

You can propose a patch by email of a particular file via the ```diff``` command:
```bash
diff -u backuppc-silzigan.org backuppc-silzigan.new > backuppc-silzigan.patch
```
The patch is applied (after reading and rereading it) via the command:
```bash
patch -p0 < backuppc-silzigan.patch
```


## COPYRIGHT

Copyright (C) 2011-2025, LEGI UMR 5519 / CNRS UGA G-INP, Grenoble, France

This project was originally written by
Gabriel Moreau <Gabriel.Moreau@univ-grenoble-alpes.fr>
and based on an original idea by Patrick Begou.

Licence: [GNU GPL version 2 or later](https://spdx.org/licenses/GPL-2.0-or-later.html)
